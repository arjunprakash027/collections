 /*

    no -age or 1age only age or _age
    no spacing between variables
    there are 32 keywords in c
    
*/
#include<stdio.h>

int main(){
    int age = 20;
    char name = 'a';
    float pi = 3.14;
    printf("age is %d \n",age);
    printf("pi is %f \n",pi);
    printf("character is %c \n",name);
    return 0;
}